<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        DB::table('users')->insert(array(
            0 => array(
                'name' => 'Administrator',
                'email' => 'admin@mail.com',
                'password' => bcrypt('12345')
            ),
            1 => array(
                'name' => 'Demo',
                'email' => 'demo@mail.com',
                'password' => bcrypt('12345')
            )
        ));
    }
}
