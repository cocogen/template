<!-- Main Footer -->
  <footer class="main-footer">
    <strong>Copyright &copy; {{date('Y')}} Cocogen Insurance, Inc.</strong>
    All rights reserved.
  </footer>